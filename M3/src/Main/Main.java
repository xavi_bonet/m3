package Main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import Classes.Persones.*;
import Classes.Vehicles.*;

public class Main {

	// Scanner
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		
	
		// llista de vehicles
		ArrayList<Vehicle> vehicles = new ArrayList();
		
		//Objectes
		TitularVehicle titular = new TitularVehicle();
		Conductor conductor = new Conductor();
		Cotxe cotxe = new Cotxe();
		Moto moto = new Moto();
		Camio camio = new Camio();
		
		
		// Menu Principal
		char opcioMenu = ' ';
		do{
			System.out.println("Qu� vols fer?");
			System.out.println("Crear un usuari: (1)");
			System.out.println("Crear un vehicle: (2)");
			System.out.println("Sortir: (3)"); // // L�aplicaci� ha d�acabar nomes quan s�indiqui que s�acabi
			opcioMenu = sc.next().charAt(0);
			switch (opcioMenu) {	
				case '1':
					
					titular = crearTitular();
					
					// Preguntar si vol crear una llicencia
					System.out.println("");
					System.out.println("Vols afegir una llicencia ?  [1] = SI | [2] = NO");
					if (validarSiNo()) {
						Llicencia llicencia = crearLllicencia();
						titular.setLlicenciaConduir(llicencia);
					}

					break;
					
				case '2':
					if (titular.getNom() != "") {
						menuCotxes(titular, conductor, cotxe, moto, camio, vehicles);
					} else {
						System.out.println("Crea un usuari Titular avans de crear un vehicle");
					}
					
					break;
					
				default:
					break;	
			}
		} while(opcioMenu!='3');
		
		// Mostrar Vehicles (amb totes les dades "titular, conductor, etc..") creats durant la execucio del programa
		Iterator <Vehicle> iterator = vehicles.iterator();
		Vehicle item;
		while (iterator.hasNext()) {
			item = iterator.next();
			System.out.println(item.toString());
		}
		
	}
	
	// Metodes
	
	// Menu cotxes
	public static void menuCotxes(TitularVehicle titular, Conductor conductor, Cotxe cotxe, Moto moto, Camio camio, ArrayList vehicles) {
		
		Llicencia l1 = new Llicencia();
		
		// Varibles para comprovacions
		boolean llicenciaValidaTitular; 
		boolean llicenciaValidaConductor = false;
		boolean titularConductor;
		boolean afegirConductor;
		
		char opcioMenu = ' ';
		do{
			System.out.println("Qu� vols fer?");
			System.out.println("Crear un cotxe: (1)");
			System.out.println("Crear una moto: (2)");
			System.out.println("Crear un camio: (3)");
			System.out.println("Tornar: (4)");
			opcioMenu = sc.next().charAt(0);
			switch (opcioMenu) {
			
				// COTXE
				case '1':
					
					l1 = titular.getLlicenciaConduir();
					
					// Comprovar si tens la llicencia (true = si / false = no)
					llicenciaValidaTitular = comprovarLlicencia(l1, opcioMenu); 
					// Crear un cotxe amb les dades (matr�cula del cotxe, marca i color)
					cotxe = crearCotxe(); 	
					// Crear objecte rodes amb les dades de les rodes
					Rodes rodesCotxe = crearRodesCotxeCamio();	
					// Guardad les dades de les rodes al cotxe
					cotxe.setRodes(rodesCotxe); 

					// Titular te llicencia
					if (llicenciaValidaTitular) {
						// Si titular te llicencia Preguntar si sira el conductor
						System.out.println("El titular es conductor? [1] = SI | [2] = NO");
						titularConductor = validarSiNo();
						
						// Si el titular no sera el conductor, crear un nou usuari conductor amb la llicencia per al vehicle adecuat
						if (!titularConductor) { // OPCIO NO
							System.out.println("Crea el conductor, aquest ha de tenir la llicencia per a cotxe");
							
							// Crear conductor
							conductor = crearConductor();
							
							// comprovar que la llicencia es del vehicle adecuat
							while (!llicenciaValidaConductor) { 
								
								// Crear una llicencia
								Llicencia l2 = crearLllicencia();
								// Comprovar si tens la llicencia (true = si / false = no)
								llicenciaValidaConductor = comprovarLlicencia(l2, opcioMenu);
								// Guardar la llicencia al objecte conductor
								conductor.setLlicenciaConduir(l2);
							}
							
							// Cotxe amb totes les dades, les dades del titular i les dades del conductor
							cotxe = new Cotxe(cotxe.getMatricula(), cotxe.getMarca(), cotxe.getColor(), cotxe.getRodes(), titular, conductor);
							vehicles.add(cotxe);
							
						} else { // OPCIO SI
							
							// Cotxe amb totes les dades i les dades del titular (titular es conductor)
							cotxe = new Cotxe(cotxe.getMatricula(), cotxe.getMarca(), cotxe.getColor(), cotxe.getRodes(), titular);
							vehicles.add(cotxe);
						}
					
					// Titular no te llicencia
					} else {
						// si titular no te llicencia Preguntar afegir conductor
						System.out.println("Vols afegir un conductor? [1] = SI | [2] = NO");
						afegirConductor = validarSiNo();
						
						// Si vols afegir un conductor, crear un nou usauri conductor amb la llicencia per al vehicle adecuat
						if (afegirConductor) { // OPCIO SI
							System.out.println("Crea el conductor, aquest ha de tenir la llicencia per a cotxe");
							
							// crear un conductor
							conductor = crearConductor();
							
							// comprovar que la llicencia es del vehicle adecuat
							while (!llicenciaValidaConductor) { 
								
								// Crear una llicencia
								Llicencia l2 = crearLllicencia();
								// Comprovar si tens la llicencia (true = si / false = no)
								llicenciaValidaConductor = comprovarLlicencia(l2, opcioMenu);
								// Guardar la llicencia al objecte conductor
								conductor.setLlicenciaConduir(l2);
							}
							// Cotxe amb totes les dades, les dades del titular i les dades del conductor
							cotxe = new Cotxe(cotxe.getMatricula(), cotxe.getMarca(), cotxe.getColor(), cotxe.getRodes(), titular, conductor);
							vehicles.add(cotxe);
						} else { // OPCIO NO
							// Cotxe amb totes les dades i les dades del titular (titular no es conductor)
							cotxe = new Cotxe(cotxe.getMatricula(), cotxe.getMarca(), cotxe.getColor(), cotxe.getRodes(), titular, false);
							vehicles.add(cotxe);
						}
					}
					break;
					

				// MOTO
				case '2':
					
					l1 = titular.getLlicenciaConduir();
					
					// Comprovar si tens la llicencia (true = si / false = no)
					llicenciaValidaTitular = comprovarLlicencia(l1, opcioMenu); 
					// Crear una moto
					moto = crearMoto(); 	
					// Crear objecte rodes amb les dades de les rodes
					Rodes rodesMoto = crearRodesMoto();	
					// Guardad les dades de les rodes
					moto.setRodes(rodesMoto); 

					// Titular te llicencia
					if (llicenciaValidaTitular) {
						// Si titular te llicencia Preguntar si sira el conductor
						System.out.println("El titular es conductor? [1] = SI | [2] = NO");
						titularConductor = validarSiNo();
						
						// Si el titular no sera el conductor, crear un nou usuari conductor amb la llicencia per al vehicle adecuat
						if (!titularConductor) { // OPCIO NO
							System.out.println("Crea el conductor, aquest ha de tenir la llicencia per a moto");
							
							// Crear conductor
							conductor = crearConductor();
							
							// comprovar que la llicencia es del vehicle adecuat
							while (!llicenciaValidaConductor) { 
								
								// Crear una llicencia
								Llicencia l2 = crearLllicencia();
								// Comprovar si tens la llicencia (true = si / false = no)
								llicenciaValidaConductor = comprovarLlicencia(l2, opcioMenu);
								// Guardar la llicencia al objecte conductor
								conductor.setLlicenciaConduir(l2);
							}
							
							// Moto amb totes les dades, les dades del titular i les dades del conductor
							moto = new Moto(moto.getMatricula(), moto.getMarca(), moto.getColor(), moto.getRodes(), titular, conductor);
							vehicles.add(moto);
						} else { // OPCIO SI
							
							// Moto amb totes les dades i les dades del titular (titular es conductor)
							moto = new Moto(moto.getMatricula(), moto.getMarca(), moto.getColor(), moto.getRodes(), titular);
							vehicles.add(moto);
						}
					
					// Titular no te llicencia
					} else {
						// si titular no te llicencia Preguntar afegir conductor
						System.out.println("Vols afegir un conductor? [1] = SI | [2] = NO");
						afegirConductor = validarSiNo();
						
						// Si vols afegir un conductor, crear un nou usauri conductor amb la llicencia per al vehicle adecuat
						if (afegirConductor) { // OPCIO SI
							System.out.println("Crea el conductor, aquest ha de tenir la llicencia per a moto");
							
							// crear un conductor
							conductor = crearConductor();
							
							// comprovar que la llicencia es del vehicle adecuat
							while (!llicenciaValidaConductor) { 
								
								// Crear una llicencia
								Llicencia l2 = crearLllicencia();
								// Comprovar si tens la llicencia (true = si / false = no)
								llicenciaValidaConductor = comprovarLlicencia(l2, opcioMenu);
								// Guardar la llicencia al objecte conductor
								conductor.setLlicenciaConduir(l2);
							}
							// Moto amb totes les dades, les dades del titular i les dades del conductor
							moto = new Moto(moto.getMatricula(), moto.getMarca(), moto.getColor(), moto.getRodes(), titular, conductor);
							vehicles.add(moto);
						} else { // OPCIO NO
							// Moto amb totes les dades i les dades del titular (titular no es conductor)
							moto = new Moto(moto.getMatricula(), moto.getMarca(), moto.getColor(), moto.getRodes(), titular, false);
							vehicles.add(moto);
						}
					}
					break;
					
				// CAMIO
				case '3':
					
					l1 = titular.getLlicenciaConduir();
					
					// Comprovar si tens la llicencia (true = si / false = no)
					llicenciaValidaTitular = comprovarLlicencia(l1, opcioMenu); 
					// Crear un camio amb les dades (matr�cula del cotxe, marca i color)
					camio = crearCamio(); 	
					// Crear objecte rodes amb les dades de les rodes
					Rodes rodesCamio = crearRodesCotxeCamio();	
					// Guardad les dades de les rodes
					camio.setRodes(rodesCamio); 

					// Titular te llicencia
					if (llicenciaValidaTitular) {
						// Si titular te llicencia Preguntar si sira el conductor
						System.out.println("El titular es conductor? [1] = SI | [2] = NO");
						titularConductor = validarSiNo();
						
						// Si el titular no sera el conductor, crear un nou usuari conductor amb la llicencia per al vehicle adecuat
						if (!titularConductor) { // OPCIO NO
							System.out.println("Crea el conductor, aquest ha de tenir la llicencia per a camio");
							
							// Crear conductor
							conductor = crearConductor();
							
							// comprovar que la llicencia es del vehicle adecuat
							while (!llicenciaValidaConductor) { 
								
								// Crear una llicencia
								Llicencia l2 = crearLllicencia();
								// Comprovar si tens la llicencia (true = si / false = no)
								llicenciaValidaConductor = comprovarLlicencia(l2, opcioMenu);
								// Guardar la llicencia al objecte conductor
								conductor.setLlicenciaConduir(l2);
							}
							
							// Camio amb totes les dades, les dades del titular i les dades del conductor
							camio = new Camio(camio.getMatricula(), camio.getMarca(), camio.getColor(), camio.getRodes(), titular, conductor);
							vehicles.add(camio);
						} else { // OPCIO SI
							
							// camio amb totes les dades i les dades del titular (titular es conductor)
							camio = new Camio(camio.getMatricula(), camio.getMarca(), camio.getColor(), camio.getRodes(), titular);
							vehicles.add(camio);
						}
					
					// Titular no te llicencia
					} else {
						// si titular no te llicencia Preguntar afegir conductor
						System.out.println("Vols afegir un conductor? [1] = SI | [2] = NO");
						afegirConductor = validarSiNo();
						
						// Si vols afegir un conductor, crear un nou usauri conductor amb la llicencia per al vehicle adecuat
						if (afegirConductor) { // OPCIO SI
							System.out.println("Crea el conductor, aquest ha de tenir la llicencia per a camio");
							
							// crear un conductor
							conductor = crearConductor();
							
							// comprovar que la llicencia es del vehicle adecuat
							while (!llicenciaValidaConductor) { 
								
								// Crear una llicencia
								Llicencia l2 = crearLllicencia();
								// Comprovar si tens la llicencia (true = si / false = no)
								llicenciaValidaConductor = comprovarLlicencia(l2, opcioMenu);
								// Guardar la llicencia al objecte conductor
								conductor.setLlicenciaConduir(l2);
							}
							// camio amb totes les dades, les dades del titular i les dades del conductor
							camio = new Camio(camio.getMatricula(), camio.getMarca(), camio.getColor(), camio.getRodes(), titular, conductor);
							vehicles.add(camio);
						} else { // OPCIO NO
							// camio amb totes les dades i les dades del titular (titular no es conductor)
							camio = new Camio(camio.getMatricula(), camio.getMarca(), camio.getColor(), camio.getRodes(), titular, false);
							vehicles.add(camio);
						}
					}
					break;
					
					
				default:
					break;	
			}
		} while(opcioMenu!='4');
	}
	
	// Crear un Conductor 
	public static Conductor crearConductor() {
		System.out.println("Introdueix el teu nom:");
		String nom = sc.next();
		System.out.println("Introdueix els teus cognoms:");
		String cognoms = sc.next();
		System.out.println("Introdueix la teva data de naixement:");
		String dataNaixement = sc.next();
		Conductor conductor = new Conductor(nom, cognoms, dataNaixement);
		return conductor;
	}
		
	// comprovar llicencia i de quin tipus es
	public static boolean comprovarLlicencia(Llicencia llicencia, char opcioMenu) {
		int opcio = Character.getNumericValue(opcioMenu);
		if (llicencia != null) {
			String tipus = llicencia.getTipusLlicencia();
			if (opcio == 1) { // cotxe
				if (tipus.equals("cotxe")) {
					System.out.println("llicencia per a usar un cotxe (SI)");
					return true;
				} else {
					System.out.println("llicencia per a usar un cotxe (NO)");
					return false;
				}
			} else if (opcio == 2) { // moto
				if (tipus.equals("moto")) {
					System.out.println("llicencia per a usar un moto (SI)");
					return true;
				} else {
					System.out.println("llicencia per a usar un moto (NO)");
					return false;
				}
			} else if (opcio == 3) { // camio
				if (tipus.equals("camio")) {
					System.out.println("llicencia per a usar un camio (SI)");
					return true;
				} else {
					System.out.println("llicencia per a usar un camio (NO)");
					return false;
				}
			}
		}
		System.out.println("No tens cap llicencia adecuada per aquest vehicle");
		return false;
	}
	
	// Crear titular
	public static TitularVehicle crearTitular() {
		System.out.println("Introdueix el teu nom:");
		String nom = sc.next();
		System.out.println("Introdueix els teus cognoms:");
		String cognoms = sc.next();
		System.out.println("Introdueix la teva data de naixement:");
		String dataNaixement = sc.next();
		System.out.println("Dispons de garatge: [1] = SI | [2] = NO");
		boolean garatge = validarGaratgeSeguro();
		System.out.println("Dispons de seguro: [1] = SI | [2] = NO");
		boolean seguro = validarGaratgeSeguro();
		TitularVehicle titular = new TitularVehicle(nom, cognoms, dataNaixement, garatge, seguro);
		return titular;
	}
	
	// Crear llicencia
	public static Llicencia crearLllicencia() {
		System.out.println("Introdueix el id de la teva llicencia:");
		int id = Integer.parseInt(sc.next());
		System.out.println("De quin tipus es la teva llicencia: [1] = Cotxe | [2] = Moto | [3] = Camio");
		String tipusLlicencia = validarTipusLlicencia();
		System.out.println("Introdueix el nom de la llicencia:");
		String nomComplet = sc.next();
		System.out.println("Introdueix la data de caducitat de la llicencia:");
		String dataCaducitat = sc.next();
		Llicencia llicencia = new Llicencia(id, tipusLlicencia, nomComplet, dataCaducitat);
		return llicencia;
	}

	// validar tipus de llicencia 
	public static String validarTipusLlicencia() {
		String tipusLlicencia = "";
		boolean control = false;
		int dato = 0;
		while (!control) {
			dato = Integer.parseInt(sc.next());
			if (dato == 1) {
				System.out.println("Has seleccionat Cotxe");
				tipusLlicencia = "cotxe";
				control = true;
			} else if (dato == 2) {
				System.out.println("Has seleccionat Moto");
				tipusLlicencia = "moto";
				control = true;
			} else if (dato == 3) {
				System.out.println("Has seleccionat Camio");
				tipusLlicencia = "camio";
				control = true;
			} else {
				System.out.println("El numero introduit no es valid");
				control = false;
			}
		}
		return tipusLlicencia;
	}

	// Validar per a boolean de garatge i seguro
	public static boolean validarGaratgeSeguro() {
		boolean control = false;
		boolean validar = false;
		int dato = 0;
		while (!control) {
			dato = Integer.parseInt(sc.next());
			if (dato == 1) {
				System.out.println("Has seleccionat SI");
				validar = true;
				control = true;
			} else if (dato == 2) {
				System.out.println("Has seleccionat NO");
				validar = false;
				control = true;
			} else {
				System.out.println("El numero introduit no es valid");
				control = false;
			}
		}
		return validar;
	}

	// Validar SI NO
	public static boolean validarSiNo() {
		boolean validar = false;
		boolean control = false;
		int dato = 0;
		while (!control) {
			dato = Integer.parseInt(sc.next());
			if (dato == 1) {
				System.out.println("Has seleccionat SI");
				validar = true;
				control = true;
			} else if (dato == 2) {
				System.out.println("Has seleccionat NO");
				validar = false;
				control = true;
			} else {
				System.out.println("El numero introduit no es valid");
				control = false;
			}
		}
		return validar;
	}
	
	// Demanar dades i crear un objecte de tipus COTXE
	public static Cotxe crearCotxe() {

		// Demanar dades
		System.out.println("Introdueix la matricula del cotxe:");
		String matricula = comprovarMatricula();
		System.out.println("Introdueix la marca del cotxe:");
		String marca = sc.next();
		System.out.println("Introdueix el color del cotxe:");
		String color = sc.next();

		// Crear cotxe
		Cotxe cotxe = new Cotxe(matricula, marca, color);
		return cotxe;
	}

	// Demanar dades i crear un objecte de tipus MOTO
	public static Moto crearMoto() {

		// Demanar dades
		System.out.println("Introdueix la matricula de la moto:");
		String matricula = comprovarMatricula();
		System.out.println("Introdueix la marca de la moto:");
		String marca = sc.next();
		System.out.println("Introdueix el color de la moto:");
		String color = sc.next();

		// Crear cotxe
		Moto moto = new Moto(matricula, marca, color);
		return moto;
	}
	
	// Demanar dades i crear un objecte de tipus CAMIO
	public static Camio crearCamio() {

		// Demanar dades
		System.out.println("Introdueix la matricula del camio:");
		String matricula = comprovarMatricula();
		System.out.println("Introdueix la marca del camio:");
		String marca = sc.next();
		System.out.println("Introdueix el color del camio:");
		String color = sc.next();

		// Crear cotxe
		Camio camio = new Camio(matricula, marca, color);
		return camio;
	}
	
	// Demanar dades de les rodes i crear un objecte de tipus RODES per al COTXE i el CAMIO
	public static Rodes crearRodesCotxeCamio() {

		// Demanar dades
		System.out.println("Introdueix la marca de les rodes delanteres:");
		String marcaRodesD = sc.next();
		System.out.println("Introdueix el diametre de les rodes delanteres");
		double diametreRodesD = comprovarDiametre();
		System.out.println("Introdueix la marca de les rodes traseres:");
		String marcaRodesT = sc.next();
		System.out.println("Introdueix el diametre de les rodes traseres:");
		double diametreRodesT = comprovarDiametre();

		// Crear rodes
		Rodes rodesCotxe = new Rodes(marcaRodesD, diametreRodesD, marcaRodesD, diametreRodesD, marcaRodesT,
				diametreRodesT, marcaRodesT, diametreRodesT);
		return rodesCotxe;
	}

	// Demanar dades de les rodes i crear un objecte de tipus RODES per a la MOTO
	public static Rodes crearRodesMoto() {

		// Demanar dades
		System.out.println("Introdueix la marca de la roda delantera:");
		String marcaRodaD = sc.next();
		System.out.println("Introdueix el diametre de la roda delantera");
		double diametreRodaD = comprovarDiametre();
		System.out.println("Introdueix la marca de la roda trasera:");
		String marcaRodaT = sc.next();
		System.out.println("Introdueix el diametre de la roda trasera:");
		double diametreRodaT = comprovarDiametre();

		// Crear rodes
		Rodes rodesCotxe = new Rodes(marcaRodaD, diametreRodaD, marcaRodaT, diametreRodaT);
		return rodesCotxe;
	}

	// Comprovar que la matricula es valida
	public static String comprovarMatricula() {
		boolean testMatricula = false;
		String matricula = "";
		while (!testMatricula) {
			matricula = sc.next();
			if (Vehicle.validarMatricula(matricula)) {
				System.out.println("La matricula es valida");
				testMatricula = true;
			} else {
				System.out.println("La matricula no es valida");
				testMatricula = false;
			}
		}
		return matricula;
	}

	// Comprovar que el diametre es valid
	public static double comprovarDiametre() {
		boolean testDiametre = false;
		double diametre = 0;
		while (!testDiametre) {
			diametre = Double.parseDouble(sc.next());
			if (Rodes.validarDiametre(diametre)) {
				System.out.println("El diametre es valid");
				testDiametre = true;
			} else {
				System.out.println("El diametre no es valid");
				testDiametre = false;
			}
		}
		return diametre;
	}

}
