package Classes.Persones;

public class Conductor extends Persona {
	
    // Atributs
    protected Llicencia llicenciaConduir;

    
    // Constructors
    public Conductor() {
        super();
        this.llicenciaConduir = null;
    }
    
    public Conductor(String nom, String cognom, String dataNaixement) {
        super(nom, cognom, dataNaixement);
        this.llicenciaConduir = null;
    }

    public Conductor(String nom, String cognom, String dataNaixement, Llicencia llicenciaConduir) {
        super(nom, cognom, dataNaixement);
        this.llicenciaConduir = llicenciaConduir;
    }


    // Getters i Setters
	public Llicencia getLlicenciaConduir() {
		return llicenciaConduir;
	}
	public void setLlicenciaConduir(Llicencia llicenciaConduir) {
		this.llicenciaConduir = llicenciaConduir;
	}

	
	@Override
	public String toString() {
		return "Conductor [llicenciaConduir=" + llicenciaConduir + "]";
	}
    
}
