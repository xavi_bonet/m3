package Classes.Persones;

public class Llicencia {
	
    // Atributs
    private int id;
    private String tipusLlicencia;
    private String nomComplet;
    private String dataCaducitat;

    
    // Constructors
    public Llicencia() {
        this.id = 0;
        this.tipusLlicencia = "";
        this.nomComplet = "";
        this.dataCaducitat = "";
    }

    public Llicencia(int id, String tipusLlicencia, String nomComplet, String dataCaducitat) {
        this.id = id;
        this.tipusLlicencia = tipusLlicencia;
        this.nomComplet = nomComplet;
        this.dataCaducitat = dataCaducitat;
    }

    
    // Getters i Setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipusLlicencia() {
		return tipusLlicencia;
	}
	public void setTipusLlicencia(String tipusLlicencia) {
		this.tipusLlicencia = tipusLlicencia;
	}
	public String getNomComplet() {
		return nomComplet;
	}
	public void setNomComplet(String nomComplet) {
		this.nomComplet = nomComplet;
	}
	public String getDataCaducitat() {
		return dataCaducitat;
	}
	public void setDataCaducitat(String dataCaducitat) {
		this.dataCaducitat = dataCaducitat;
	}

    
	@Override
	public String toString() {
		return "Llicencia [id=" + id + ", tipusLlicencia=" + tipusLlicencia + ", nomComplet=" + nomComplet + ", dataCaducitat=" + dataCaducitat + "]";
	}

}
