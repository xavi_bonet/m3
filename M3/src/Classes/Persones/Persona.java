package Classes.Persones;

public class Persona {
	
    // Atributs
    protected String nom;
    protected String cognom;
    protected String dataNaixement;

    // Constructors
    public Persona() {
        this.nom = "";
        this.cognom = "";
        this.dataNaixement = "";
    }

    public Persona(String nom, String cognom, String dataNaixement) {
        this.nom = nom;
        this.cognom = cognom;
        this.dataNaixement = dataNaixement;
    }
    
    // Getters i Setters
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognom() {
		return cognom;
	}
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	public String getDataNaixement() {
		return dataNaixement;
	}
	public void setDataNaixement(String dataNaixement) {
		this.dataNaixement = dataNaixement;
	}

	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", cognom=" + cognom + ", dataNaixement=" + dataNaixement + "]";
	}

    
    

}
	

