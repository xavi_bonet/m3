package Classes.Persones;

public class TitularVehicle extends Persona {
	
    // Atributs
    protected Llicencia llicenciaConduir;
    protected boolean garatge;
    protected boolean seguro;

    // Constructors
    public TitularVehicle() {
        super();
        this.llicenciaConduir = null;
        this.garatge = false;
        this.seguro = false;
    }
    
    public TitularVehicle(String nom, String cognom, String dataNaixement, boolean garatge, boolean seguro) {
        super(nom, cognom, dataNaixement);
        this.llicenciaConduir = null;
        this.garatge = garatge;
        this.seguro = seguro;
    }

    public TitularVehicle(String nom, String cognom, String dataNaixement, Llicencia llicenciaConduir, boolean garatge, boolean seguro) {
        super(nom, cognom, dataNaixement);
        this.llicenciaConduir = llicenciaConduir;
        this.garatge = garatge;
        this.seguro = seguro;
    }

	// Getters i Setters
	public Llicencia getLlicenciaConduir() {
		return llicenciaConduir;
	}
	public void setLlicenciaConduir(Llicencia llicenciaConduir) {
		this.llicenciaConduir = llicenciaConduir;
	}
	public boolean isGaratge() {
		return garatge;
	}
	public void setGaratge(boolean garatge) {
		this.garatge = garatge;
	}
	public boolean isSeguro() {
		return seguro;
	}
	public void setSeguro(boolean seguro) {
		this.seguro = seguro;
	}

	@Override
	public String toString() {
		return "TitularVehicle [llicenciaConduir=" + llicenciaConduir + ", garatge=" + garatge + ", seguro=" + seguro + "]";
	}
    
	
}
