package Classes.Vehicles;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Classes.Persones.*;

public class Vehicle {
	
	// Atributs
	protected String matricula;
	protected String marca;
	protected String color;
	protected boolean titularEsConductor;
	protected TitularVehicle titularVehicle;
	protected Conductor conductors;
	
	
	// Constructos
	public Vehicle() {
		this.matricula = "";
		this.marca = "";
		this.color = "";
	}
	
	public Vehicle(String matricula, String marca, String color) {
		this.matricula = matricula;
		this.marca = marca;
		this.color = color;
	}
	
	public Vehicle(String matricula, String marca, String color, TitularVehicle titularVehicle) {
		this.matricula = matricula;
		this.marca = marca;
		this.color = color;
		this.titularEsConductor = true;
		this.titularVehicle = titularVehicle;
		this.conductors = null;
	}
	
	public Vehicle(String matricula, String marca, String color, TitularVehicle titularVehicle, boolean titularEsConductor) {
		this.matricula = matricula;
		this.marca = marca;
		this.color = color;
		this.titularEsConductor = titularEsConductor;
		this.titularVehicle = titularVehicle;
		this.conductors = null;
	}
	
	public Vehicle(String matricula, String marca, String color, TitularVehicle titularVehicle, Conductor conductors) {
		this.matricula = matricula;
		this.marca = marca;
		this.color = color;
		this.titularEsConductor = false;
		this.titularVehicle = titularVehicle;
		this.conductors = conductors;
	}

	// Getters i Setters
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	
	// Metodes
	
	// Comprovar que la matricula es valida (4 numeros | 2-3 lletres)
	public static boolean validarMatricula(String matricula) {
		boolean comprovarMatricula = false;
		Pattern pattern = Pattern.compile("^\\d{4}[A-Z]{2,3}");
        Matcher matcher = pattern.matcher(matricula);
        if (matcher.matches()) {
            comprovarMatricula = true;
        } else {
        	comprovarMatricula = false;
        }
		return comprovarMatricula;
	}

	@Override
	public String toString() {
		return "Vehicle [matricula=" + matricula + ", marca=" + marca + ", color=" + color + ", titularEsConductor="
				+ titularEsConductor + ", titularVehicle=" + titularVehicle + ", conductors=" + conductors + "]";
	}
	
	
}
