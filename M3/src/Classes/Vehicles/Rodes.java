package Classes.Vehicles;

public class Rodes {

	// Atributs
	// rdd = roda delantera dreta | rde = roda delantera esquerra | rtd = roda trasera dreta | rte = roda trasera esquerra
	private String rdd_Marca;
	private double rdd_Diametre;
	private String rde_Marca;
	private double rde_Diametre;
	private String rtd_Marca;
	private double rtd_Diametre;
	private String rte_Marca;
	private double rte_Diametre;
	
	
	// Constructors
	public Rodes() {
		this.rdd_Marca = "";
		this.rdd_Diametre = 0;
		this.rde_Marca = "";
		this.rde_Diametre = 0;
		this.rtd_Marca = "";
		this.rtd_Diametre = 0;
		this.rte_Marca = "";
		this.rte_Diametre = 0;
	}
	
	public Rodes(String rd_Marca, double rd_Diametre, String rt_Marca, double rt_Diametre) {
		this.rdd_Marca = rd_Marca;
		this.rdd_Diametre = rd_Diametre;
		this.rde_Marca = "";
		this.rde_Diametre = 0;
		this.rtd_Marca = rt_Marca;
		this.rtd_Diametre = rt_Diametre;
		this.rte_Marca = "";
		this.rte_Diametre = 0;
	}
	
	public Rodes(String rdd_Marca, double rdd_Diametre, String rde_Marca, double rde_Diametre, String rtd_Marca, double rtd_Diametre, String rte_Marca, double rte_Diametre) {
		this.rdd_Marca = rdd_Marca;
		this.rdd_Diametre = rdd_Diametre;
		this.rde_Marca = rde_Marca;
		this.rde_Diametre = rde_Diametre;
		this.rtd_Marca = rtd_Marca;
		this.rtd_Diametre = rtd_Diametre;
		this.rte_Marca = rte_Marca;
		this.rte_Diametre = rte_Diametre;
	}


	// Getters i Setters
	public String getRdd_Marca() {
		return rdd_Marca;
	}
	public void setRdd_Marca(String rdd_Marca) {
		this.rdd_Marca = rdd_Marca;
	}
	public double getRdd_Diametre() {
		return rdd_Diametre;
	}
	public void setRdd_Diametre(double rdd_Diametre) {
		this.rdd_Diametre = rdd_Diametre;
	}
	public String getRde_Marca() {
		return rde_Marca;
	}
	public void setRde_Marca(String rde_Marca) {
		this.rde_Marca = rde_Marca;
	}
	public double getRde_Diametre() {
		return rde_Diametre;
	}
	public void setRde_Diametre(double rde_Diametre) {
		this.rde_Diametre = rde_Diametre;
	}
	public String getRtd_Marca() {
		return rtd_Marca;
	}
	public void setRtd_Marca(String rtd_Marca) {
		this.rtd_Marca = rtd_Marca;
	}
	public double getRtd_Diametre() {
		return rtd_Diametre;
	}
	public void setRtd_Diametre(double rtd_Diametre) {
		this.rtd_Diametre = rtd_Diametre;
	}
	public String getRte_Marca() {
		return rte_Marca;
	}
	public void setRte_Marca(String rte_Marca) {
		this.rte_Marca = rte_Marca;
	}
	public double getRte_Diametre() {
		return rte_Diametre;
	}
	public void setRte_Diametre(double rte_Diametre) {
		this.rte_Diametre = rte_Diametre;
	}

	
	// Metodes
	@Override
	public String toString() {
		return "Rodes [rdd_Marca=" + rdd_Marca + ", rdd_Diametre=" + rdd_Diametre + ", rde_Marca=" + rde_Marca + ", rde_Diametre=" + rde_Diametre + ", rtd_Marca=" + rtd_Marca + ", rtd_Diametre=" + rtd_Diametre + ", rte_Marca=" + rte_Marca + ", rte_Diametre=" + rte_Diametre + "]";
	}
	

	
	// Comprovar diametre rodes (superior a 0.4 i inferior a 4)
	public static boolean validarDiametre(double diametreRodes) {
		boolean comprovarDiametre = false;
        if (diametreRodes > 0.4 && diametreRodes < 4) {
            comprovarDiametre = true;
        }
        return comprovarDiametre;
	}
	
}
