package Classes.Vehicles;

import Classes.Persones.Conductor;
import Classes.Persones.TitularVehicle;

public class Camio extends Vehicle{

	// Atributs
	private Rodes rodes;
	
	
	// Constructors
	public Camio() {
		super();
		this.rodes = null;
	}
	
	public Camio(String matricula, String marca, String color) {
		super(matricula, marca, color);
		this.rodes = null;
	}
	
	public Camio(String matricula, String marca, String color, Rodes rodes) {
		super(matricula, marca, color);
		this.rodes = rodes;
	}
	
	public Camio(String matricula, String marca, String color, Rodes rodes, TitularVehicle titularVehicle) {
		super(matricula, marca, color, titularVehicle);
		this.rodes = rodes;
	}
	
	public Camio(String matricula, String marca, String color, Rodes rodes, TitularVehicle titularVehicle, boolean titularEsConductor) {
		super(matricula, marca, color, titularVehicle, titularEsConductor);
		this.rodes = rodes;
	}
	
	public Camio(String matricula, String marca, String color, Rodes rodes, TitularVehicle titularVehicle, Conductor conductors) {
		super(matricula, marca, color, titularVehicle, conductors);
		this.rodes = rodes;
	}
	
	// Getters i Setters
	public Rodes getRodes() {
		return rodes;
	}
	public void setRodes(Rodes rodes) {
		this.rodes = rodes;
	}

	// Metodes
	@Override
	public String toString() {
		return "Camio [rodes=" + rodes + ", matricula=" + matricula + ", marca=" + marca + ", color=" + color
				+ ", titularEsConductor=" + titularEsConductor + ", titularVehicle=" + titularVehicle + ", conductors="
				+ conductors + "]";
	}
	
	
	

	
}
